import { extractFromFile } from "./extract/mod.ts";

export async function loadWakeRefs(refDir: string) {
  const wakeDir = await Deno.readDir(refDir);
  const files: number[][][] = [];
  const names: string[] = [];
  for await (const file of wakeDir) {
    if (file.isFile) {
      names.push(file.name);
      const data = await extractFromFile(`${refDir}/${file.name}`);
      files.push(data.map((entry) => entry.feature));
    }
  }
  return { names, files };
}
