export async function* fileIterable<T>(stream: Deno.FsFile) {
  const reader = stream.readable.getReader();

  try {
    while (true) {
      const { done, value } = await reader.read();
      if (done) return;
      yield value;
    }
  } finally {
    reader.releaseLock();
  }
}
