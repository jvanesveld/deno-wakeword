export async function* streamWindows(
  input: Iterable<number[]> | AsyncIterable<number[]>,
  windowSize = 1024,
  offset = 512
) {
  let accum: number[] = [];
  for await (const data of input) {
    accum = accum.concat(data);
    while (accum.length >= windowSize) {
      const window = accum.slice(0, windowSize);
      if (sum(...window) === 0) {
        window[0] = 1;
      }
      yield window;
      accum.splice(0, offset);
    }
  }
}

export async function createWindows(
  data: number[],
  windowSize = 1024,
  offset = 512
) {
  const windows: number[][] = [];
  for await (const window of streamWindows([data], windowSize, offset)) {
    windows.push(window);
  }
  return windows;
}

function sum(...values: number[]) {
  let res = 0;
  for (const value of values) {
    res += value;
  }
  return res;
}
