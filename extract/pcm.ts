import { createWindows } from "./window.ts";

export function arrayFrom16LE(audio: Uint8Array) {
  const result = [];
  const view = new DataView(audio.buffer);
  const length = audio.length - (audio.length % 2);
  for (let i = 0; i < length; i += 2) {
    const data = view.getUint16(i, true);
    result.push(data);
  }
  return result;
}
export function toUint8(audio: number[]) {
  const buffer = new ArrayBuffer(audio.length * 2);
  const view = new DataView(buffer);
  for (const [i, data] of audio.entries()) {
    view.setUint16(i * 2, data, true);
  }
  return new Uint8Array(buffer);
}

export function toMono(audio: number[]) {
  const result: number[] = [];
  for (let i = 0; i < audio.length; i += 2) {
    result.push(audio[i] / 2 + audio[i + 1] / 2);
  }
  return result;
}

function getLoudness(audioFrame: number[]) {
  let total = 0;
  for (const data of audioFrame) {
    total += data * data;
  }
  return total / audioFrame.length;
}

export async function trim(
  audio: number[],
  silenceRatio = 20,
  frameSize = 128,
  frameOffset = 64
) {
  const windows = await createWindows(audio, frameSize, frameOffset);
  const energies = [];
  for (const window of windows) {
    const loud = getLoudness(window);
    energies.push(loud);
  }
  let start = 0;
  let end = 0;
  const powerMultiplier = Math.pow(10, silenceRatio / 10);
  const maxEnergy = Math.max(...energies);
  const silenceThreshold = maxEnergy / powerMultiplier;
  for (const [i, energy] of energies.entries()) {
    if (start === 0 && energy >= silenceThreshold) {
      start = i;
    }
    if (energy >= silenceThreshold) {
      end = i + 1;
    }
  }
  return audio.slice(start * frameSize, end * frameSize);
}
