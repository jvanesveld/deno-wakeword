import FFT from "https://esm.sh/fft.js@4.0.4";
import { construct } from "./mel.js";

export const SAMPLE_RATE = 48000;

const config = {
  fftSize: 1024,
  bankCount: 24,
  lowFrequency: 50,
  highFrequency: 8000,
  sampleRate: SAMPLE_RATE,
};

export const mfcc = construct(config, 40);

export function fftMagnitude(data: number[]) {
  const f = new FFT(data.length);
  const out = f.createComplexArray();
  f.realTransform(out, data);
  const result = [];
  for (let i = 0; i < out.length; i += 2) {
    result.push(Math.sqrt(out[i] * out[i] + out[i + 1] * out[i + 1]));
  }
  return result;
}
