import { fftMagnitude, mfcc } from "./mfcc.ts";
import { arrayFrom16LE, toMono, toUint8, trim } from "./pcm.ts";
import { streamWindows } from "./window.ts";

export async function* extractStream(
  audio: Iterable<number[]> | AsyncIterable<number[]>
) {
  const windows = streamWindows(audio);
  let i = 0;
  for await (const window of windows) {
    i++;
    const fftMag = fftMagnitude(window);
    const result = mfcc(fftMag) as number[];
    yield {
      feature: result.map((value) => value),
      raw: toUint8(window),
    };
  }
}

function mean(values: number[]) {
  let total = 0;
  for (const value of values) {
    total += value;
  }
  return total / values.length;
}

export function extract(audio: number[]) {
  return collect(extractStream([audio]));
}

export async function extractFromFile(path: string, preprocess = false) {
  const file = await Deno.readFile(path);
  if (!preprocess) {
    return extract(arrayFrom16LE(file));
  }
  return extract(await preprocessData(file));
}

export function preprocessData(data: Uint8Array) {
  const newData = arrayFrom16LE(data);
  const mono = toMono(newData);
  return trim(mono);
}

export async function* preprocessStream(
  data: Iterable<Uint8Array> | AsyncIterable<Uint8Array>
) {
  for await (const raw of data) {
    const newData = arrayFrom16LE(raw);
    const mono = toMono(newData);
    yield mono;
  }
}

export async function collect<T>(data: Iterable<T> | AsyncIterable<T>) {
  const result: T[] = [];
  for await (const value of data) {
    result.push(value);
  }
  return result;
}
