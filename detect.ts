import { loadWakeRefs } from "./load-data.ts";
import { extractStream, preprocessStream } from "./extract/mod.ts";
import { SAMPLE_RATE } from "./extract/mfcc.ts";
import { DynamicTimeWarping } from "./deps.ts";

const numberBytes = (duration: number) => (duration * SAMPLE_RATE) / 1000;

function cosineDist(a: number[], b: number[]) {
  const magRes = mag(a) * mag(b);
  const dist = 1 - dot(a, b) / magRes;
  return dist;
}

function dot(a: number[], b: number[]) {
  if (a.length !== b.length) {
    throw "Dot with unequal length vectors";
  }
  let result = 0;
  for (let i = 0; i < a.length; i++) {
    result += a[i] * b[i];
  }
  return result;
}

function mag(a: number[]) {
  let result = 0;
  for (let i = 0; i < a.length; i++) {
    result += a[i] * a[i];
  }
  return Math.sqrt(result);
}

// const values: number[] = [];
function getProbability(ref1: number[][], ref2: number[][], max: number) {
  const offset = (max + max) / (ref1.length + ref2.length);
  const dtw = new DynamicTimeWarping(ref1, ref2, cosineDist);
  const dist = dtw.getDistance() * 10;
  return (offset - dist) / offset;
}

export async function* detect(
  audio: Iterable<Uint8Array> | AsyncIterable<Uint8Array>,
  refDir: string,
  threshold = 0.8
) {
  const { names, files } = await loadWakeRefs(refDir);
  const pStream = preprocessStream(audio);
  const fStream = extractStream(pStream);
  let features: number[][] = [];
  const maxFrames = Math.max(...files.map((file) => file.length));
  for await (const result of fStream) {
    features.push(result.feature);
    if (features.length > maxFrames) {
      features.splice(0, features.length - maxFrames);
    } else {
      continue;
    }

    let maxProb = 0;
    let name = "";
    for (const [i, ref] of files.entries()) {
      const prob = getProbability(ref, features, maxFrames);
      if (maxProb < prob) {
        maxProb = prob;
        name = names[i];
      }
    }
    if (maxProb > threshold) {
      features = [];
      yield { prob: maxProb, name };
    }
  }
}
