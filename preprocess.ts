import { fileIterable } from "./extract/file.ts";
import { collect, preprocessStream } from "./extract/mod.ts";
import { toUint8, trim } from "./extract/pcm.ts";

const from = `./source`;
const to = `./`;
const wakeDir = await Deno.readDir(from);

for await (const file of wakeDir) {
  if (file.isFile && !file.name.startsWith("mono")) {
    const openedFile = await Deno.open(`${from}/${file.name}`, { read: true });
    const stream = fileIterable(openedFile);
    const data = await collect(preprocessStream(stream));
    const trimmed = await trim(data.flat());
    Deno.writeFile(`${to}/mono-${file.name}`, toUint8(trimmed));
  }
}
